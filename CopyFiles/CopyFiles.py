# Program : To copy all the data text files from a direcotry to model a LM.
# Language : Python3
# Author : S.Moris
# Version: 1

# Import necessary Libraries
import os
import sys
# Give the directoy as a command line input.
directory = sys.argv[1] # Direcotry where the txt is present
output_dir = sys.argv[2] # The output direcotry for the data/text files.
for file in os.listdir(directory):
    if file.endswith(".txt"):
        print("The file is :",file)
        if os._exists(directory + '/' + "unique_words_" + file + '.list'):
            print("The unique wordlist exists.")
            os.popen('cp '+ directory + '/' +  file + ' ' +  directory + '/' + output_dir + '/' + file )
            print("The unique words is : ",len(open(directory + '/' + "unique_words_" + file + '.list').readlines()))