# Version : 1.1
# Import necessary Libraries.
import os
import sys
import numpy as np
# Give the text direcotry as input.
directory =  sys.argv[1]
list_directory = sys.argv[2]
output = sys.argv[3]
# Set variables to create Dev.txt
for file in os.listdir(directory):
    if 0.9 * len(open(directory + '/' + file,'r').readlines()) > len(open(list_directory + '/' + "unique_words_" + file + '.list','r').readlines()):
         with open (output + '/' + 'dev_' + file,'a') as dev :
            with open(directory + '/' + file,'r+') as text:
                lines = text.readlines()
                numbers = np.random.random_integers(0,len(lines),size=(1,int(0.1*len(lines))))
                print("Writing dev.txt from :",file)
                text.seek(0)
                for i,line in enumerate(lines):
                    if i in numbers:
                        dev.write(line +'\n')
                    else:
                        text.write(line)
                text.truncate()
    else:
        with open (output + '/' + 'dev_' + file,'a') as dev :
            with open(directory + '/' + file,'r') as text:
                lines = text.readlines()
                numbers = np.random.random_integers(0,len(lines),size=(1,int(0.1*len(lines))))
                print("Writing dev.txt without truncating from :",file)
                for i,line in enumerate(lines):
                    if i in numbers:
                        dev.write(line +'\n')

os.popen('cat ' + output + '/*.txt >' + directory + '/dev.txt' )
